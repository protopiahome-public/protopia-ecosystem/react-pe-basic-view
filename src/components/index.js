import React, { Component} from "react"
import axios from "axios"
import { menu, concatRouting } from "react-pe-layouts"
import { __ } from "react-pe-utilities"
import {LayoutIcon} from 'react-pe-useful'
import {byId } from "react-pe-layouts"
import { initArea } from "react-pe-utilities"
import Feed from "./Feed"
import Style from "style-it"
import Layouts from "react-pe-layouts"
import chroma from "chroma-js"

class BasicState extends Component {
  constructor(props) {
    super(props)
    // console.log(this.basic_state_data());
    const newStyle = byId(this.props.style_id)
    const isLeft = typeof this.props.is_left !== "undefined" ? this.props.is_left : null
    // console.log( newStyle );
    if ((newStyle || isLeft) && this.props.onChangeStyle) 
      this.props.onChangeStyle({ fluid: 1, style: newStyle.url, isLeft })
    this.state = {
      ...this.basic_state_data(),
      html: "",
      route: {
        variables: this.props.variables ? this.props.variables : {},
        icon: this.props.icon ? this.props.icon : "usercor-light",
        title: this.props.title ? this.props.title : this.getTitle(),
        html: this.props.html ? this.props.html : "",
        panel: this.props.panel ? this.props.panel : "",
        html_sourse: this.props.html_sourse ? this.props.html_sourse : "",
        data_type: this.props.data_type ? this.props.data_type : "",
      },
      description: "",
      panelHtml: "",
    }
  }

  componentDidMount() {
    this.setState({
      route: this.layout(),
      panelHtml: this.myPanelHtml(this.layout()),
      html: this.myState(this.layout()),
    })
    this.stateDidMount()
  }

  stateDidMount() {

  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.html || nextProps.icon || nextProps.title || nextProps.html_sourse || nextProps.data_type) {
      const { route } = this.state
      // console.log(nextProps);
      this.setState({
        html: this.myState(nextProps),
        panelHtml: this.myPanelHtml(nextProps),
        route: {
          ...route,
          icon: nextProps.icon,
          title: nextProps.title,
          html: nextProps.html,
          html_sourse: nextProps.html_sourse,
          panel: nextProps.panel,
          data_type: nextProps.data_type,
        },
      })
    }
  }

  alternateRender() {
    return null
  }

  addRender() {
    return null
  }

  beforeRender() {
    return null
  }
  getExtendParams = () =>
  {
    return this.props.extend_params
      ?
      {...Layouts().template.extend_params, ...this.props.extend_params}
      :
      Layouts().template.extend_params
  }

  render() {
    const extendParams = this.getExtendParams()
    // console.log(this.props)
    // console.log( extendParams )
    const alt = this.alternateRender()
    if (alt) return alt
    this.beforeRender()

    return Style.it(
      `
      .layout-state,
      .pe
      {
        background-color : ${extendParams?.backgroundColor};
        color: ${extendParams?.color};
        padding:0;
      }  
      .pe-bg
      {
        background-color : ${extendParams?.backgroundColor}!important;
      }
      .pe-bg-hover
      {
        transition: background-color easy-out 200ms;
      }
      .pe-bg-hover:hover
      {
        background-color : ${ chroma( extendParams?.backgroundColor ).darken( .22 ).alpha( .33 ) }!important;
      }
      .layout-state-logo,
      .layout-state-title
      {
        color: ${extendParams?.color}!important;
      }
      .layout-state-bg
      {
        background-image: ${
          extendParams?.bgThumbnail 
            ? 
            extendParams?.bgThumbnail.indexOf("http") === 0
              ?
              `url(${extendParams?.bgThumbnail})`
              :
              extendParams?.bgThumbnail 
            : 
            null
        };
        filter: ${
          extendParams?.bgBlur 
              ? 
              `blur(${extendParams?.bgBlur}px)` 
              : 
              null   
        };
        opacity:${extendParams?.bgOpacity || 1};
      }
      .menu-aside,
      .tutor-main-2 .clapan-left,
      .menu-elem,
      .card,
      .card-header,
      .bp3-card,
      .pe-card
      {
        background-color: ${ chroma( extendParams?.cardColor ).darken( .11 ) }!important;
        color: ${ extendParams?.cardTextColor }!important;
      }
      .card:hover,
      .menu-elem:hover
      {
        background-color: ${extendParams?.cardColor}!important; 
      }
      .stroke-a-dark a, .a-dark,
      .stroke-a-dark:hover a, .a-dark:hover,
      .bp3-button.bp3-minimal,
      .bp3-button:not([class*="bp3-intent-"]),
      .bp3-tab,
      .dark .bp3-input,
      .page-item.active .page-link,
      .datetimer input,
      .text-dark,
      .pe-by-bg
      {
        color: ${extendParams?.color}!important;
      }
      .layout-state-description
      {
        color: ${ chroma(extendParams?.color).alpha(0.75) }!important;
      }
      .bp3-tab[aria-selected="true"], 
      .bp3-tab:not([aria-disabled="true"]):hover 
      {
        color: #106ba3!important;
      }
      .bg-light
      {
        background-color: ${extendParams?.cardColor}!important;
      }
      .bp3-minimal .pe-by-bg svg,
      .pe-by-bg.bp3-minimal svg
      {
        fill: ${extendParams?.color};
      } 

      `,
      <div className="layout-state" >
        <div 
          className="layout-state-bg"   
        />
        {this.renderTitle()}
        {this.addRender()}
        {this.state.html}
      </div>
    ) 
  }
  renderTitle()
  {
    return (this.state.route?.icon || this.state.route?.title) && !this.props.hide_title 
      ?
      <div className="layout-state-head">
        <LayoutIcon
          isSVG
          src={this.state.route ? this.state.route.icon : "" }
          className="layout-state-logo "
        />
        <div className="layout-state-title">
          { __(this.state.route ? this.state.route.title : "") }
        </div>
        <div className="layout-state-heaader">          
          { initArea("layout_heaader_panel", this) }
        </div>
      </div>
      :
      <div className="layout-state-head p-0 m-0">
        <div className="layout-state-heaader">          
          { initArea("layout_heaader_panel", this) }
        </div>
      </div>
  }
  rightPanel() {
    return this.state.panelHtml
  }

	getName = () => {
	  let name = menu().filter((e) => e.route === this.state.route)
	  if (!name) name = "404"
	  return name
	}

	getIcon = () => {
	  let icon = menu().filter((e) => e.icon === this.state.icon)
	  if (!icon) icon = "fas fa-user"
	  return icon
	}

	getRoute = () => this.props.route

	myPanelHtml = (route) => {
	  if (route && route.panel) return <div dangerouslySetInnerHTML={{ __html: route.panel }} />
	  return this.state.panelHtml
	}

	myState = (route) => {
	  if (route && route.html) return <div dangerouslySetInnerHTML={{ __html: route.html }} />
	  if (route && route.html_sourse) {
	    axios.get(route.html_sourse)
	      .then((response) => {
	        this.setState({ html: <div dangerouslySetInnerHTML={{ __html: response.data }} /> })
	      })
	    return ""
	  }
	  if (route && route.data_type) {
	    return ""// this.get_data( route.data_type);
	  }
	  return ""
	}

	layout = () => {
	  const name = concatRouting()
	    .filter((e) => e.route == this.getRoute())
	  return name.length > 0 ? name[0] : this.state.route
	}

	getTitle() {

	}

	basic_state_data() {
	  return {}
	}
}
export default BasicState

export {Feed}
