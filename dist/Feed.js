function _extends() { _extends = Object.assign ? Object.assign.bind() : function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }
function _defineProperty(obj, key, value) { key = _toPropertyKey(key); if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
function _toPropertyKey(arg) { var key = _toPrimitive(arg, "string"); return typeof key === "symbol" ? key : String(key); }
function _toPrimitive(input, hint) { if (typeof input !== "object" || input === null) return input; var prim = input[Symbol.toPrimitive]; if (prim !== undefined) { var res = prim.call(input, hint || "default"); if (typeof res !== "object") return res; throw new TypeError("@@toPrimitive must return a primitive value."); } return (hint === "string" ? String : Number)(input); }
import React, { Component } from "react";
import gql from "graphql-tag";
import { compose } from "recompose";
import { Query, withApollo } from "react-apollo";
import { Pagi } from "react-pe-useful";
import { Loading } from "react-pe-useful";
import { __ } from "react-pe-utilities";
import { getQueryArgs } from "react-pe-layouts";
class Feed extends Component {
  constructor(props) {
    super(props);
    _defineProperty(this, "onPagi", n => {
      this.setState({
        offset: n * this.state.count
      });
    });
    this.state = {
      count: parseInt(this.props.count) ? parseInt(this.props.count) : 5,
      full_count: -1,
      offset: parseInt(this.props.offset)
    };
  }
  componentDidMount() {
    const name = `get${this.props.data_type}Count`;
    const {
      paging
    } = this.props;
    const query = gql`
				query ${name} 
				{
					${name}( paging:{ ${paging}  }) 
				}
			`;
    this.props.client.query({
      query
    }).then(result => {
      // console.log(result.data[name]);
      this.setState({
        full_count: result.data[name]
      });
    });
  }
  render() {
    let html;
    if (this.props.data_type) {
      const name = `get${this.props.data_type}s`;
      const {
        paging
      } = this.props;
      const fields = getQueryArgs(this.props.data_type);
      const {
        count
      } = this.state;
      const {
        offset
      } = this.state;
      const {
        full_count
      } = this.state;
      const query = gql`
				query ${name} 
				{
					${name}( paging:{ count:${count}, offset:${offset}, ${paging}  })
					{
						${fields}
					}
				}
			`;
      const shifter = count && count < full_count && !this.props.is_hide_pagi ? /*#__PURE__*/React.createElement("div", {
        className: "py-1 d-flex pe-pagi "
      }, /*#__PURE__*/React.createElement(Pagi, {
        all: Math.ceil(full_count / parseInt(count)) - 1,
        current: parseInt(offset) / parseInt(count),
        onChoose: this.onPagi
      })) : null;
      html = /*#__PURE__*/React.createElement(React.Fragment, null, shifter, /*#__PURE__*/React.createElement(Query, {
        query: query
      }, ({
        loading,
        error,
        data,
        client
      }) => {
        if (loading) {
          return /*#__PURE__*/React.createElement(Loading, null);
        }
        if (data) {
          // console.log(data[name]);
          const feed = name && data[name] && data[name].length > 0 ? data[name].map((e, i) => {
            const ElComponento = this.props.component;
            return /*#__PURE__*/React.createElement(ElComponento, _extends({}, e, {
              elem: e,
              height: this.props.height
            }, this.props.params, {
              data_type: this.props.data_type,
              key: i
            }));
          }) : this.no();
          return /*#__PURE__*/React.createElement("div", {
            className: this.props.class_name,
            style: this.props.stye
          }, feed);
        }
        if (error) {}
      }));
    } else {
      html = this.no();
    }
    return html;
  }
  no() {
    return /*#__PURE__*/React.createElement("div", {
      className: "alert alert-secondary"
    }, __("No elements exist"));
  }
}
export default compose(withApollo)(Feed);